#include <stdlib.h>
#include <stdio.h>

int main(){

	int num, div, n_div=0;


	printf("Dime un numero\n");
	scanf(" %i", &num);
	for(div=2; div<num; div++){
		if(num % div == 0){
		printf("%i es un divisor\n", div);
		n_div++;
	}
	}
	printf("Por lo tanto %i tiene %i divisores\n", num, n_div);

	return EXIT_SUCCESS;
}
