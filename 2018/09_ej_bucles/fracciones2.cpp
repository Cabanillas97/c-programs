#include <stdlib.h>
#include <stdio.h>

#define N 100


int main(){
	int n1, n2, pd1, pd2, nn1, nn2, div1[N], div2[N];

	printf("Dime una fraccion\n");
	printf("Primer numero: \n");
	scanf(" %i", &n1);
	printf("Segundo numero\n");
	scanf(" %i", &n2);
	printf("Por lo tanto tu numero es %i/%i\n", n1, n2);
	printf("Vamos a simplificarlo\n");
	
	for(pd1=2; pd1<=n1; pd1++){
	for(pd2=2; pd2<=n2; pd2++)
	if(pd1==pd2 && n1 % pd1 == 0 && n2 % pd2 == 0){
		nn1 = n1/pd1;
		nn2 = n2/pd2;
		n1 = nn1;
		n2 = nn2;
	printf("El numerador es divisible entre %i\n", pd1);
	printf("El denominador es divisible entre %i\n", pd2);	
	}
	}
	printf("Tu numero simplificado es %i/%i\n", nn1, nn2);

	return EXIT_SUCCESS;
}
