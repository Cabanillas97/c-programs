#include <stdlib.h>
#include <stdio.h>

int main(){

	unsigned primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
	unsigned elementos = (unsigned) sizeof(primo)/sizeof(int); /*(unsigned) es un molde*/  
	unsigned *peeping = primo;  /* unsigned *peeping guarda y apunta a la direccion de memoria de algo---------alli donde apunta*/
	char *tom  = (char *) primo;  /* char *   quiere decir la direccion de un char, le hacemos un molde para que solo nos devuelva un char, ya que primo es un unsigned, asi se mueve de uno en uno*/
	unsigned **police = &peeping;
	/* unsigned (polais)[9] = primo;*/


	printf(" PRIMO:\n"
		"======\n"
		"Localizacion (%p)\n"
		" Elementos: %u [%u..%u]\n"
		" Tamaño: %lu bytes.\n\n",
		primo,
	      	elementos,
	       	primo[0], primo[elementos-1],
	       	sizeof(primo));

	printf("0: %u\n", peeping[0]);
	printf("1: %u\n", peeping[1]);
	printf("0: %u\n", *peeping);
	printf("1: %u\n", *(peeping+4));
	printf("Tamaño: %lu bytes.\n", sizeof(peeping));

	/* Memory Dump - Volcado de Memoria-------Direccion de donde esta primo*/
	for (int i=0; i<sizeof(primo); i++)
	printf("%02X", *(tom + i));
	printf("\n");


	printf("Police contiene %p\n", police);
	printf("Primo contiene %p\n", *police); /*Lo que contiene alli donde apunta*/
	printf("Primo[0] contiene %u\n", **police); /*donde apunta alli donde apunta police*/
	/* Tambien se podria poner para mirar otro numero *(*police+2)*/
	return EXIT_SUCCESS;
}

