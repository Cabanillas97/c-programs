#include <stdlib.h>
#include <stdio.h>

#define OUT "frases.txt"
#define N 10
#define MAX_LIN 0x10000

int main(int argc, char *argv[]){

	const char * edit;
	FILE *pf;
	char buffer[MAX_LIN];
			/*La w es el modo de escritura*/
	if(!(pf=fopen (OUT, "w")) ){
		fprintf(stderr, "No he podido abrir %s\n", OUT);
		return EXIT_FAILURE;
	}
	printf("Dinos las %i frases mas importantes de tu vida\n", N);
		for (int i=0; i<N; i++)	{
			printf("Frase: ");
			fgets(buffer, MAX_LIN, stdin);
			fprintf(pf, buffer);
		}
			
	fclose(pf);


	return EXIT_SUCCESS;
}
