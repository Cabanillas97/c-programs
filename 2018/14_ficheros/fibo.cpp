#include <stdlib.h>
#include <stdio.h>

#define FILENAME "fibo.dat"

int main(){

	int fibo[13]={0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144};	
	FILE *pf = NULL;
	int n = sizeof(fibo) / sizeof(int);

	if(! (pf = fopen (FILENAME, "wb"))){
		fprintf(stderr, "No se puede abrir el fichero\n");	
	}

	fwrite(fibo, sizeof(int), n, pf);
	fclose(pf);


	return EXIT_SUCCESS;
}

/*hacer a mano la sucesion de fibonacci en un array y volcarlo en un fichero binario usando fwrite, abrirlo en escritura binaria wb.*/
