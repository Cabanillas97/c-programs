#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#define N 0x100
void print_usage(FILE *outstream, int exit_code){
	fprintf(outstream, "\
			usage:\t %s[options]\n\
			\n\
				-Replace all of the occurrences of seek characters (s) with\n\
				 Replace character (r) within file (f).
			Options: \n\
			-f <nombredefichero>\t defaults to stdin.\n\
			-r <character>\t defaults to '!'\n\
			-s <character>\t defaults to 'i\n\
			-h
			\n\
			Example: \n
				%s -rd -sb -fa.txt\n\
				\n\

			", progname, progname);
	exit(exit_code)

}


int main(){

	int c;
	progname = argv[0];
	char r ='!';/*char to replace with*/
	char s ='i';/*char to look for*/
	char f ='-';/*file name. '-' equals to stdin*/
	char filename[N];

					/*help fichero :que tiene argumento replace que tiene argumento*/
	while ( (c = getopt (argc, argv, "hf:r:s")) !=-1 ){
		switch(c){
			case 'h':
				print_usage(stdout, 0);
				break;
			case 'f':
				/*./tradfich -f a.txt // optarg points to a in a.txt*/
				/*la n significa como mucho n bites, existe tambien strcpy*/
				strncpy(filename, optarg, n);
						/*optarg es la direccion donde esta ese argumento opcional
						 * argumento de la opcion*/
				f='\0';
			break;
			case 'r':
				r = *optarg;
			break;
			case 's':
				s = *optarg;
			break;
			case '?':
				if(optpot !='f' && optopt !='r' && optopt !='s')
					fprintf (stderr, "Invalid option.\n");
				else
					fprintf(stderr, "Option argument missing.\n");
				print_usage(stderr, 1);
				break;
			default:
				abort();

		}
	}



	return EXIT_SUCCESS;
}

/*para sacar ayudas k mayuscula*/
