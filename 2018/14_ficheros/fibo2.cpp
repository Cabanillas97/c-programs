#include <stdlib.h>
#include <stdio.h>
#define N 9
#define FILENAME "fibo.dat"
int rellenar(int fi[N], int i){
	if(i==1){
	
		fi[0] = 1;
		return fi[1]=1;
	}
	return fi[i] = rellenar (fi, i-1) + fi[i-2];
}


int main(){

	int fibo[N];	
	FILE *pf = NULL;
	
	rellenar(fibo, N-1);

	int n = sizeof(fibo) / sizeof(int);

	if(! (pf = fopen (FILENAME, "wb"))){
		fprintf(stderr, "No se puede abrir el fichero\n");	
	}

	fwrite(fibo, sizeof(int), n, pf);
	fclose(pf);


	return EXIT_SUCCESS;
}
/*od -tx nombrefichero*/
