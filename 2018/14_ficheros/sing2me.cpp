#include <stdlib.h>
#include <stdio.h>

#define CANCION "cancion.txt"

int main(int argc, char *argv[]){

	const char * cancion;
	char c;
	FILE *pf;
	if(argc < 2)
		return EXIT_FAILURE;

	cancion = argv[1];
	if( !(pf=fopen (CANCION, "r")) )
		return EXIT_FAILURE;

	while( (c = fgetc (pf)) != EOF)
		printf("%c", c);
	return EXIT_SUCCESS;
}


/*alt gr ñ se cambian de mayusculas a minusculas y viceversa*/
