#include <stdlib.h>
#include <stdio.h>
#define MAX 0x100

/*Partes de una funcion: 1.Declaracion de variables
 * 			 2.Definicion de la funcion
 * 			 3.Llamada (dos tipos de parametros, formales y actuales
 * 			 4.Parametros(Formales y actuales)*/

void pon_titulo(int op /*Parámetro formal*/){
	char titulo[MAX];
	/*Título*/
	sprintf(titulo, "toilet -fpagga --metal Tabla del %i",op);
	/*Con sprintf hacemos que toilet -fpagga .... se guarde en titulo*/
	system(titulo);
	
/*Las variables de una funcion no existen en otra funcion*/
}


int main(){
	/*Variables*/
	int op1, op2, resultado;
	
	/*Menú*/
	printf("Dime el numero del que quieres la tabla\n");
	scanf(" %i", &op1);
	
	/*Título*/
	pon_titulo(op1);

	/*Resultados*/
	for(op2=0; op2<11; op2++){
		resultado = op1 * op2;
		printf("%i*%i=%i\n", op1, op2, resultado);
	}

	return EXIT_SUCCESS;
}
