#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

/*alt 1 alt2 se cambia de ventana*/
int main(){

	/*printf tiene escritura bufferizada*/
	fprintf(stderr, "hola\n");	/*mirar esta tarde*/
	fputs("hola", stderr);
	fprintf(stderr, "\a");		/* stderr no esta buferizado*/
	usleep(20);
	fputc('\a', stderr);	
	usleep(20);
	printf("\a\n");
	
	
	return EXIT_SUCCESS;
}


/*con usleep se pone una decima de segundo*/
