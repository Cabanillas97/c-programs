#include <stdlib.h>
#include <stdio.h>

#define N 100
#define DELAY 100000

int main(){

	for(int veces=0; veces<N; veces++){
		for(int columna=0; columna<veces; columna++)
			fprintf(stderr, "=");
		fprintf(stderr, "> %i%%\r", veces);
		usleep(DELAY);
	}
	printf("\n");
	
	return EXIT_SUCCESS;
}
