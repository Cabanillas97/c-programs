#include <stdlib.h>
#include <stdio.h>

#define MAX 0x100

int main(){
	/*Variables*/
	int op1, op2, resultado;
	char titulo[MAX];
	
	/*Menú*/
	printf("Dime el numero del que quieres la tabla\n");
	scanf(" %i", &op1);
	/*Título*/
	sprintf(titulo, "toilet -fpagga --metal Tabla del %i",op1);/*Con sprintf hacemos que toilet -fpagga .... se guarde en titulo*/
	system(titulo);
	
	/*Resultados*/
	for(op2=0; op2<11; op2++){
		resultado = op1 * op2;
		printf("%i*%i=%i\n", op1, op2, resultado);
	}

	return EXIT_SUCCESS;
}
