#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define VECES 24
#define BTU 100000	/*Basic Time Unit*/

int main(){

	int duracion[VECES] = {0, 2, 8, 2, 1, 8, 2, 2, 2, 2, 1, 3, 5, 2, 1, 2, 1, 1, 1, 2, 3, 4, 3, 2};


	for(int i=0; i<VECES; i++){
		usleep(duracion[i] * BTU);
		fputc('\a', stderr);
	}



	return EXIT_SUCCESS;
}
