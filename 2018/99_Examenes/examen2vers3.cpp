#include <stdlib.h>
#include <stdio.h>

#define M 2
#define K 4
#define N 3
/* * significa lo que hay alli donde apunta  
 * 5 9 2 3 5 7
 * *m+1 = 6 (5 + 1)
 * *(m+1) = 9 (posicion m donde esta el 5 == m[0], + 1 que es donde esta el 9  == m[1])
 * FORMULA DE DIRECCIONAMIENTO DE UNA MATRIZ
 *  	*(i*c+j)
 * */
/* Función punto de entrada */
void imprime(double m[][N], int f){
	for(int i=0; i<f; i++){
		for(int j=0; j<N; j++)
			printf("\t%.2lf", m[i][j]);
		printf("\n");
}
}


int  main(){

	int i, j;
	double A[M][K] = {
		{2, 3, 5, 1},
		{3, 1, 4, 2}
	},
	       B[K][N] = {
		       {5,  2, 1},
		       {3, -7, 2},
		       {-4, 5, 1},
		       {2, 3, -9}
	       },
	       C[M][N];

	for(int i=0; i<M; i++){
		for(int j=0; j<N; j++){
			C[i][j] = 0;
			for (int k=0; k<K; k++)
				C[i][j] += A[i][k] * B[k][j];
		}
	}
	imprime(B, 4);
	printf("\n\n");
	imprime(C, 2);

	return EXIT_SUCCESS;
}
