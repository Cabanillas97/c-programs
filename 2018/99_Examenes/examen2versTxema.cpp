#include <stdlib.h>
#include <stdio.h>

#define M 2
#define K 4
#define N 3

/* Función punto de entrada */
/* const se pone para que no cambie ninguna variable de lo que hayamos puesto en titulo*/
void imprime(double *m, int f, int c, const char *titulo){
	printf("%s:\n\n", titulo);
	for(int i=0; i<f; i++){
		for(int j=0; j<c; j++)
			printf("\t%7.2lf", *(m + i *c + j));
		printf("\n");
}
}


int  main(){

	int i, j;
	double A[M][K] = {
		{2, 3, 5, 1},
		{3, 1, 4, 2}
	},
	       B[K][N] = {
		       {5,  2, 1},
		       {3, -7, 2},
		       {-4, 5, 1},
		       {2, 3, -9}
	       },
	       C[M][N];

	for(int i=0; i<M; i++){
		for(int j=0; j<N; j++){
			C[i][j] = 0;
			for (int k=0; k<K; k++)
				C[i][j] += A[i][k] * B[k][j];


			/*imprime(C);*/
		}
		/*imprime(C);*/
	}
	imprime((double *) A, M, K, "Matriz A");
	printf("\n");
	imprime((double *) B, K, N, "Matriz B");
	printf("\n");
	imprime((double *) C, M, N, "Matriz C");
		/*Molde*/
	return EXIT_SUCCESS;
}
