#include <stdlib.h>
#include <stdio.h>

#define M 2
#define K 4
#define N 3

/* Función punto de entrada */
int  main(){

	int i, j, fila;
	double A[M][K] = {
		{2, 3, 5, 1},
		{3, 1, 4, 2}
	},
	       B[K][N] = {
		       {5,  2, 1},
		       {3, -7, 2},
		       {-4, 5, 1},
		       {2, 3, -9}
	       },
	       C[M][N];





	/*	printf ("Fila: ");
		scanf (" %i", &i);
		printf ("Columna: ");
		scanf (" %i", &j);*/
	
	for(int i=0; i<M; i++){
		for(int j=0; j<N; j++){
			C[i][j] = 0;
			for (int k=0; k<K; k++)
				C[i][j] += A[i][k] * B[k][j];
		/*Forma1*/
			printf ("\t%.2lf", C[i][j]);

		}
		printf("\n");
	}

	/*Forma2*/
/*	for(int i=0; i<M; i++){
		for(int j=0; j<N; j++)
			printf("\t%.2lf", C[i][j]);
		printf("\n");
	}*/

	return EXIT_SUCCESS;
}
