#include <stdlib.h>
#include <stdio.h>

int main(){
	int l;
	printf("¿Cuantas filas quieres para el triangulo?\n");
	scanf(" %i", &l);
	
	for (int f=0; f<l; f++){
		for(int c=0; c<=f; c++)
			printf(" *");
		printf("\n");
	}


	return EXIT_SUCCESS;
}
