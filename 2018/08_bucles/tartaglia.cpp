#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#define N 10
unsigned dv(unsigned celda[N][N], int f, int c){
	if(f<0||c<0||c>f)
		return (unsigned) 0;
	return celda[f][c];

}


int main(){

	unsigned matriz[N][N];
	bzero(matriz, sizeof(matriz));
	matriz[0][0] = 1;
	for(int fila=1; fila<N; fila++)
		for(int col=0; col<=fila; col++)
			matriz[fila][col] = dv(matriz, fila-1, col-1) + dv(matriz, fila-1, col);



	  for(int fila=1; fila<N; fila++){
		for(int col=0; col<=fila; col++)
			printf(" %i", matriz[fila][col]);
		printf("\n");
		}

/*	matriz[N][M] = matriz[N-1][M] + matriz[N-1][M-1];
	printf(" %i", matriz[N][M]);
*/
	return EXIT_SUCCESS;
}

