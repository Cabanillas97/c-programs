#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void dimePrimo(int n){
	int d=0;
	for(int div=1; div<=n; div++){
		if (n % div == 0)
			d++;
	}
	if (d<=2)
		printf("Es primo\n");
	else
		printf("No es primo\n");
}


int main(){
	int numero; 

	printf("¿Que numero quieres comprobar?\n");
	scanf("%i", &numero);
	dimePrimo(numero);

	return EXIT_SUCCESS;
}
