#include <stdlib.h>
#include <stdio.h>
#define N 3
int main(){
int res1, res2, determinante;
int matriz[N][N];

	printf("Dime la matriz [3][3]\n");
	for(int i=0; i<N; i++){
	scanf(" %i %i %i", &matriz[i][0], &matriz[i][1], &matriz[i][2]);
	}
	
	printf("Tu matriz es: \n");
	
	for(int i=0; i<N; i++)
		printf("%i %i %i\n", matriz[i][0], matriz[i][1], matriz[i][2]);

	res1=(matriz[0][0] * matriz[1][1] * matriz[2][2])+
	(matriz[1][0] * matriz[2][1] * matriz[0][2])+
	(matriz[2][0] * matriz[0][1] * matriz[1][2]);
	
	printf("Tu resultado es %i\n", res1);

	res2=(matriz[0][2] * matriz[1][1] * matriz[2][0])+
	(matriz[1][2] * matriz[2][1] * matriz[0][0])+
	(matriz[2][2] * matriz[0][1] * matriz[1][0]);
	
	printf("Tu resultado es %i\n", res2);
	
	determinante=res1-res2;
	
	printf("El determinante es %i\n", determinante);
	return EXIT_SUCCESS;
}
