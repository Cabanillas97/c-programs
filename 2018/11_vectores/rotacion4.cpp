#include <stdlib.h>
#include <stdio.h>
#include <math.h>
/*matriz[f][c]
 *  *c pabajo
 *   *fila ---- */
int main(){
	int lado = 0;
	double ncoord, grados;
	/*der*/	double a[2][2]/*={
				{cos, sin},
				{-sin, cos}
				}
				*/;
		/*izq*/ double b[2][2]/*={
					{cos, -sin},
					{sin, cos}
					}*/;
		double c[2][1]/*={
				{x},
				{y}*/;
		printf("¿Hacia que lado quieres la rotacion?\n"
				"Derecha = 0, Izquierda = 1\n");
	scanf(" %i", &lado);

	switch(lado){
		case 0: printf("Dime la coordenada X: \n");
			scanf(" %lf", &c[0][0]);
			printf("Dime la coordenada Y: \n");
			scanf(" %lf", &c[1][0]);
			printf("Dime cuantos grados quieres girarlo: \n");
			scanf("%lf", &grados);
			a[0][0]=cos(grados*M_PI/180);
			a[1][1]=a[0][0];
			a[0][1]=sin(grados*M_PI/180);
			a[1][0]=-a[0][1];

			for(int i=0; i<2; i++){

				ncoord = a[0][i] * c[0][0] + a[1][i] * c[1][0];

				printf("La nueva coordenada es %lf\n", ncoord);
			}
			break;

		case 1:
			printf("Dime la coordenada X: \n");
			scanf(" %lf", &c[0][0]);
			printf("Dime la coordenada Y: \n");
			scanf(" %lf", &c[1][0]);
			printf("Dime cuantos grados quieres girarlo: \n");
			scanf("%lf", &grados);
			b[0][0]=cos(grados*M_PI/180);
			b[1][1]=b[0][0];
			b[1][0]=sin(grados*M_PI/180);
			b[0][1]=-b[1][0];

			for(int i=0; i<2; i++){

				ncoord = b[0][i] * c[0][0] + b[1][i] * c[1][0];

				printf("La nueva coordenada es %lf\n", ncoord);
			}
			break;
		default:
			printf("0 o 1 unicamente\n");


	}

	return EXIT_SUCCESS;
}

