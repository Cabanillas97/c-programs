#include <stdlib.h>
#include <stdio.h>
#define N 3
int main(){
int res1, res2, res3, resfin, resfin2, resfin3;
int matriz[N][N]={
	{2, 3, 4},
	{3, 8, 9},
	{2, 5, 7}
};

	printf("Tenemos una matriz de 3X3,\n"
			"2	3	4\n"
			"3	8	9\n"
			"2	5	7\n");
	
	
	res1=(matriz[0][0] * matriz[1][1] * matriz[2][2]);
	printf("%i\n", res1);
	res2=(matriz[1][0] * matriz[2][1] * matriz[0][2]);
	printf(" %i\n", res2);
	res3=(matriz[2][0] * matriz[0][1] * matriz[1][2]);
	printf(" %i\n", res3);
	resfin = res1+res2+res3;
	printf("El resultado es %i\n", resfin);
	res1=(matriz[0][2] * matriz[1][1] * matriz[2][0]);
	printf(" %i\n", res1);
	res2=(matriz[1][2] * matriz[2][1] * matriz[0][0]);
	printf(" %i\n", res2);
	res3=(matriz[2][2] * matriz[0][1] * matriz[1][0]);
	printf(" %i\n", res3);
	resfin2= res1 + res2 + res3;
	printf("El resultado es %i\n", resfin2);
	resfin3 = resfin - resfin2;
	printf("El determinante es %i\n", resfin3);
	return EXIT_SUCCESS;
}
