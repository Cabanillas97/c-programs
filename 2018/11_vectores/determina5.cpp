#include <stdlib.h>
#include <stdio.h>
#define N 3
int main(){
	double multiplicacion, determinante1 = 0, determinante2 = 0, resulfin;
	double matriz[N][N]={	
		{2, 3, 4},
		{3, 8, 9},
		{2, 5, 7}
	};

	for(int f=0; f<N; f++){
		multiplicacion= 1;
		for(int d=0; d<N; d++)
			multiplicacion *= matriz[(f+d)%N][0+d];
		determinante1 += multiplicacion;
		/* *=  lo que habia por */
	}
	printf("Resultado: %2.lf\n", determinante1);

	for(int f=0; f<N; f++){
		multiplicacion= 1;
		for(int d=0; d<N; d++)
			multiplicacion *= matriz[(f+d)%N][2-d];
		determinante2 += multiplicacion;
		/* *=  lo que habia por */
	}
	printf("Resultado: %2.lf\n", determinante2);

	resulfin=determinante1 - determinante2;
	printf("El determinante es: %2.lf\n", resulfin);

	return EXIT_SUCCESS;
}
