#include <stdlib.h>
#include <stdio.h>
#define N 10
void eleva(int *a){
	
	for(int i=0; i<N; i++)
	a[i]= i*i;
}


int main(){

	int array[N];
	eleva(array);
	for(int i=0; i<N; i++){
	printf("La potencia del a[%i] es %i\n", i, array[i]);
	}

	return EXIT_SUCCESS;
}

