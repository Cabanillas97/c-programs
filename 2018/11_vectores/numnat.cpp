#include <stdlib.h>
#include <stdio.h>
#define N 10

int main(){

	int array[N]{0,1,2,3,4,5,6,7,8,9};
	int potencia;
	
	for(int i=0; i<N; i++){
	potencia = array[i]*array[i];
	printf("La potencia del a[%i] es %i\n", i, potencia);
	}

	return EXIT_SUCCESS;
}




