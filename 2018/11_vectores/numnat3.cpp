#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define N 10
void eleva(int *a){
	
	for(int i=0; i<N; i++)
	a[i]=pow(i,2);
}


int main(){

	int array[N];
	eleva(array);
	for(int i=0; i<N; i++){
	printf("La potencia del a[%i] es %i\n", i, array[i]);
	}

	return EXIT_SUCCESS;
}

